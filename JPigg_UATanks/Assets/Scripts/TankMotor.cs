﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(CharacterController))] // This requires a CharacterController component to be placed on this game object. This is done
                                                // to prevent errors when trying to call characterController functions without a reference to a controller.
public class TankMotor : MonoBehaviour
{
    // private variables
    private CharacterController characterController; // This will hold the CharacterController component of this gameObject
    private Transform tankTransform; // This will hold the gameobject's transform component
    public AudioClip fireSound;
    public AudioMixerGroup mixerGroup;

    // Use this for initialization
    void Start()
    {
        characterController = gameObject.GetComponent<CharacterController>(); // Sets the variable equal to the CharacterController component of 
                                                                              // the gameObject this script is attached to.

        tankTransform = gameObject.GetComponent<Transform>(); // Sets the tankTransform variable equal to the transform component of the gameobject
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Move(float speed) // uses the characterController SimpleMove function to move the tank forward and backwards
    {
        Vector3 moveDirection = tankTransform.forward * speed; // This creates a vector 3 to store the forward of the transform multiplied by the speed

        characterController.SimpleMove(moveDirection); // The moveDirection is then passed into the SimpleMove function allowing the tank to move forward
                                                       // and backwards along its local z-axis
    }

    public void Turn(float speed) // uses the rotate function of the transform componente to rotate our tank around the z-axis on a per-second basis
    {
        tankTransform.Rotate(new Vector3(0, speed * Time.deltaTime, 0), Space.Self); // We only want the tanks to rotate around the y axis, so we can just
                                                                                     // pass our speed argument into the y of the vector and normalize it
                                                                                     // with Time.deltaTime
    }

    public void Shoot(GameObject shellPrefab, float force, Transform cannonPosition) // Instatiates a shell at the end of the barrel of the tank with a passed force
    {
        GameObject instantiatedShell = Instantiate(shellPrefab, cannonPosition.position, Quaternion.identity); // instatiates the shell and keeps a reference to it in the
                                                                                                               // instatiatedShell variable
        instantiatedShell.GetComponent<DestroyFiredShells>().SetParentData(this.gameObject); // get the DestroyFiredShells script off of the shell that was fired and call the
                                                                                             // SetParentData function passing in this gameobject
        instantiatedShell.GetComponent<Rigidbody>().AddForce(force * tankTransform.forward, ForceMode.Impulse);// adds force to the referenced instatiated object of the shell prefab
        if (fireSound != null)
        {
            GameObject oneShotSound = new GameObject("One Shot Sound"); // create a gameobject
            oneShotSound.transform.position = transform.position; // set its position to this one's
            oneShotSound.AddComponent<AudioSource>(); // add an audio source
            AudioSource source = oneShotSound.GetComponent<AudioSource>(); // get that audio source
            source.clip = fireSound; //  set the clip to be the fire sound
            source.outputAudioMixerGroup = mixerGroup; // set the mixer to be the fx mixer
            source.volume = 0.7f; // set the volume to 70%
            source.Play(); // play the clip
            Destroy(oneShotSound, fireSound.length); // destroy the gameobject after the clip
        }
    }

    public bool RotateTowards(Vector3 target, float speed)
    {
        Vector3 vectorToTarget;

        // The vector to our target is the Difference between the target position and our position.
        vectorToTarget = target - tankTransform.position;

        // find the quaternion that looks down that vector
        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);

        // If that is the direction we are already looking, we don't need to turn!
        if (targetRotation == tankTransform.rotation)
        {
            return false;
        }

        // Otherwise:
        // Change our rotation so that we are closer to our target rotation, but never turn faster than our Turn Speed
        //  Note that we use Time.deltaTime because we want to turn in "Degrees per Second" not "Degrees per Framedraw"
        tankTransform.rotation = Quaternion.RotateTowards(tankTransform.rotation, targetRotation, speed * Time.deltaTime);

        // We rotated, so return true
        return true;
    }
}
