﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{
    [HideInInspector] public List<Powerup> powerups; // stores the active powerups
    TankData data; //referece to this tank's tankdata script
    // Use this for initialization
    void Start()
    {
        powerups = new List<Powerup>(); // create a new list
        data = gameObject.GetComponent<TankData>(); // set data equal to tankdata
    }

    // Update is called once per frame
    void Update()
    {
        List<Powerup> expiredPowerups = new List<Powerup>(); // create a list of powerups that are expiring this frame

        foreach (Powerup power in powerups) // cycle through all active powerups
        {
            power.duration -= Time.deltaTime; // and reduce their duration

            if (power.duration <= 0) // if the time is up
            {
                expiredPowerups.Add(power); // add this to the expired powerups list
            }
        }

        foreach (Powerup power in expiredPowerups) // cycle through all expired powerups
        {
            power.OnDeactivate(data); // call the deactivate function for each expired powerup
            powerups.Remove(power); // remove from the active powerups list
        }
        expiredPowerups.Clear(); // clear out the expired powerups
    }

    public void Add(Powerup powerup)
    {
        powerup.OnActivate(data); // calls the activate function to add the powerup abilities

        if (!powerup.isPermanent) // if the powerup isn't permanent
        {
            powerups.Add(powerup); // add it to our list, so we can keep track of it
        }
    }
}
