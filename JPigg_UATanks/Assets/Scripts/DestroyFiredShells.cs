﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class DestroyFiredShells : MonoBehaviour
{
    TankData parentData; // Holds the reference to the tankData script from the gameObject that shot the shell
    float DestroyTime; // the time which this object will be destroyed if it doesnt hit something first
    public AudioClip hitSound; // the sound clip to play when the shell hits a player
    public AudioMixerGroup mixerGroup; // the mixer group this sound is played to. Used to adjust fx sound independently of music

    // Use this for initialization
    void Start()
    {
        DestroyTime = Time.time + parentData.shellTimeout; // the destroy time equals the time this was created + the timeout from the TankData script
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= DestroyTime) // destroys the object when the current time is greater than or equal to DestroyTime
        {
            Destroy(gameObject); // Destroys the gameobject this is attached to
        }
    }

    void OnCollisionEnter(Collision other) // called when the shell collides with something
    {
        if (other.gameObject.tag == "Tank" || other.gameObject.tag == "Player") // if the tag on the other object is Tank
        {
            int score;	// holds the score that other tank is worth when destroyed
            score = other.gameObject.GetComponent<TankData>().TakeDamage(parentData.attackDamage); // calls the TakeDamage function on the other tank's TankData script
                                                                                                   // and passes in the amount of damage this shell is supposed to do from
                                                                                                   // its parent object's TankData attackDamage variable

            if (hitSound != null) // if we have a sound 
            {
                GameObject oneShotSound = new GameObject("One Shot Sound"); // create a gameobject to host the sound
                oneShotSound.transform.position = transform.position; // set it at our position
                oneShotSound.AddComponent<AudioSource>(); // create an audio source on this new gameobject
                AudioSource source = oneShotSound.GetComponent<AudioSource>(); // get that audiosource we just created
                source.clip = hitSound; // set the clip to be our hit sound
                source.outputAudioMixerGroup = mixerGroup; //set our output to be the mixergroup
                source.volume = 0.7f; // set our volume at 70%
                source.Play(); // play the sound
                Destroy(oneShotSound, hitSound.length); //destroy the game object after the sound is done
            }
            if (score != 0) // if the score != 0, meaning the other tank was destroyed. Gives the possibility to also have negative score for friendly fire
            {
                parentData.AddScore(score); // calls the AddScore function on the parent object's TankData script, allowing each tank to keep their own score
            }
        }
        Destroy(gameObject); // destroys the gameobject this is attached to after a collision
    }

    public void SetParentData(GameObject passedobject) // sets the gameobject which instatiated the shell as the "parentData"
    {
        parentData = passedobject.GetComponent<TankData>(); // gets a reference to the parents TankData script for damage and timeout variables
    }

}
