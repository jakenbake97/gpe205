﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;
using System;

public class MenuManager : MonoBehaviour
{
    public AudioMixer audioMixer; // holds a reference to the audio mixer so volume can update it
    GameManager gameManagerRef; // reference to the gamemanager instance

    void Awake()
    {
        gameManagerRef = GameManager.instance; // set the instance reference
    }

    public void SetGenerationMethod(int generationType) // based on the dropdown in option the generation type is determined
    {
        if (generationType == 0) // 0 is map of day
        {
            gameManagerRef.genType = GameManager.GenerationType.MapOfDay;
        }
        else if (generationType == 1) // 1 is random
        {
            gameManagerRef.genType = GameManager.GenerationType.Random;
        }
        else // 2 is seeded
        {
            gameManagerRef.genType = GameManager.GenerationType.Seeded;
        }
    }

    public void SetMapSeed(string seedInput) // sets the map seed based on string input
    {
        int result;
        if (int.TryParse(seedInput, out result)) // try to parse the seed from string to int
        {
            gameManagerRef.mapSeed = result; // if we can, the seed equals the entered seed
        }
        else
        {
            gameManagerRef.mapSeed = 1; // otherwise default to 1
        }
    }

    public void SetNumberOfPlayers(bool multiplayer) // a togle for multiplayer
    {
        if (multiplayer) // if the toggle is true
        {
            gameManagerRef.playersToSpawn = 2; // tell gamemanger that it should spawn two players

        }
        else
        {
            gameManagerRef.playersToSpawn = 1; // otherwise it should just spawn 1

        }
    }

    public void SetFXVolume(float volume) // set the fx volume in the mixer
    {
        audioMixer.SetFloat("fxVolume", volume); // uses an exposed parameter to set the volume based on the slider position
    }

    public void SetMusicVolume(float volume) // set the music volume in the mixer
    {
        audioMixer.SetFloat("musicVolume", volume); // user an exposed parameter to set the volume based on the slider position
    }

    public void QuitGame() // called when the quit buton is pressed
    {
        Application.Quit(); // if in a built version of the game the progrtam will quit when this is called
    }

    public void SetPlayer1Controls(int controlScheme) // sets the controls of player 1 based on what the dropdown says
    {
        gameManagerRef.playerControls[0] = controlScheme; // the player controls for player one equal the dropdown
    }
    public void SetPlayer2Controls(int controlScheme) // sets the controls of player 2 based on what the dropdown says
    {
        gameManagerRef.playerControls[1] = controlScheme;
    }

    public void SetGridSizeX(string XSize) // the size the grid will be based on input
    {
        int result;
        if (int.TryParse(XSize, out result)) //if we can parse the input
        {
            gameManagerRef.cols = result; // then set the columns to that size
        }
    }

    public void SetGridSizeY(string YSize) // the size the grid will be based on input
    {
        int result;
        if (int.TryParse(YSize, out result)) //if we can parse the input
        {
            gameManagerRef.rows = result; // then set the rows to that size
        }
    }

    public void SetDifficulty(int difficultyLevel) // the dropdown for difficulty
    {
        gameManagerRef.difficulty = difficultyLevel + 1; // sets the dificulty level between 1 and 5
    }
}
