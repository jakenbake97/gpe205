﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup
{
    public float speedModifier; // changes the speed of the tank
    public float healthModifier; // changes the health of the tank
    public float maxHealthModifier; // changes the max health of the tank
    public float fireRateModifier; // changes the reload rate of the tank
    public float duration; // how long the paowerup lasts for
    public float respawnDelay; // how long it takes for the powerup to come back
    public bool isPermanent; // if the powerup is permanent its effects won't be negated
    float startingDuration; // remembers what the duration was when it counts down

    private void Start()
    {
        startingDuration = duration; // set the starting duration equal to the duration for this powerup
    }

    public void OnActivate(TankData target) // when the powerup is activated
    {
        target.moveSpeed += speedModifier; // change the move speed to reflect the modifier
        target.tankHealth += healthModifier; // change the tankHealth to reflect the modifier
        target.startingHealth += maxHealthModifier; // change the starting health to reflect the new max health
        target.reloadTime += fireRateModifier; // change the reload time to reflect the fire rate modifier
    }

    public void OnDeactivate(TankData target) // when the powerup is deactivated subtract any changes
    {
        target.moveSpeed -= speedModifier;
        target.tankHealth -= healthModifier;
        target.startingHealth -= maxHealthModifier;
        target.reloadTime -= fireRateModifier;
        duration = startingDuration;
    }
}
