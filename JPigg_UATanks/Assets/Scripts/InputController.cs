﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankData))] // Require TankData component to be on the gameobject

[RequireComponent(typeof(TankMotor))] // Require TankMotor component to be on the gameobject 
public class InputController : MonoBehaviour
{
    // public variables
    public enum InputScheme { WASD, arrowKeys, IJKL, GamePad } // creates an InputScheme type which can be used for selection of different control configurations

    public InputScheme inputSelection = InputScheme.WASD; // presents options in the inspector for designers to select the perferred input method.
                                                          // A variable to use for our switch case allowing us to see the chosen input method.

    // private variables
    TankMotor motor; // holds a reference to the TankMotor script on this gameobject

    TankData data; // holds a reference to the TankData script on this gameobject
    float nextTimeToShoot;


    // Use this for initialization
    void Start()
    {
        motor = gameObject.GetComponent<TankMotor>(); // sets the motor equal to the TankMotor component

        data = gameObject.GetComponent<TankData>(); // sets the data variable equal to the TankData component
        nextTimeToShoot = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        // runs through the different InputScheme options to determine which is selected, and then gets that specific input for that selection
        switch (inputSelection)
        {
            case InputScheme.arrowKeys: // moves tank when arrowKeys input is selected, and a coresponding key is pressed
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    motor.Move(data.moveSpeed); //calls the Move function on the TankMotor script passing in the moveSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    motor.Move(-data.moveSpeed); //calls the Move function on the TankMotor script passing in the -moveSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    motor.Turn(data.rotationSpeed); //calls the Turn function on the TankMotor script passing in the rotationSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    motor.Turn(-data.rotationSpeed); //calls the Turn function on the TankMotor script passing in the -rotationSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.KeypadEnter))
                {
                    if (Time.time >= nextTimeToShoot) // if the current time is greater than or equal to the nextTimeToShoot then the tank can shoot
                    {
                        motor.Shoot(data.shellPrefab, data.shotForce, data.cannonPosition); // calls the shoot function on the tankMotor script passing in the
                                                                                            // shellPrefab, force, and position to instatiate a shot
                        nextTimeToShoot = Time.time + data.reloadTime; // nextTimeToShoot equals current time + the reload time held in TankData
                    }
                }
                break;
            case InputScheme.IJKL: // moves tank when IJKL input is selected, and a coresponding key is pressed
                if (Input.GetKey(KeyCode.I))
                {
                    motor.Move(data.moveSpeed); //calls the Move function on the TankMotor script passing in the moveSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.K))
                {
                    motor.Move(-data.moveSpeed); //calls the Move function on the TankMotor script passing in the -moveSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.L))
                {
                    motor.Turn(data.rotationSpeed); //calls the Turn function on the TankMotor script passing in the rotationSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.J))
                {
                    motor.Turn(-data.rotationSpeed); //calls the Turn function on the TankMotor script passing in the -rotationSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.Return))
                {
                    if (Time.time >= nextTimeToShoot) // if the current time is greater than or equal to the nextTimeToShoot then the tank can shoot
                    {
                        motor.Shoot(data.shellPrefab, data.shotForce, data.cannonPosition); // calls the shoot function on the tankMotor script passing in the
                                                                                            // shellPrefab, force, and position to instatiate a shot
                        nextTimeToShoot = Time.time + data.reloadTime; // nextTimeToShoot equals current time + the reload time held in TankData
                    }
                }
                break;
            case InputScheme.WASD: // moves tank when WASD input is selected, and a coresponding key is pressed
                if (Input.GetKey(KeyCode.W))
                {
                    motor.Move(data.moveSpeed); //calls the Move function on the TankMotor script passing in the moveSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.S))
                {
                    motor.Move(-data.moveSpeed); //calls the Move function on the TankMotor script passing in the -moveSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.D))
                {
                    motor.Turn(data.rotationSpeed); //calls the Turn function on the TankMotor script passing in the rotationSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.A))
                {
                    motor.Turn(-data.rotationSpeed); //calls the Turn function on the TankMotor script passing in the -rotationSpeed value from TankData
                }
                if (Input.GetKey(KeyCode.Space))
                {
                    if (Time.time >= nextTimeToShoot) // if the current time is greater than or equal to the nextTimeToShoot then the tank can shoot
                    {
                        motor.Shoot(data.shellPrefab, data.shotForce, data.cannonPosition); // calls the shoot function on the tankMotor script passing in the
                                                                                            // shellPrefab, force, and position to instatiate a shot
                        nextTimeToShoot = Time.time + data.reloadTime; // nextTimeToShoot equals current time + the reload time held in TankData
                    }
                }
                break;
            case InputScheme.GamePad: // moves tank when GamePad input is selected, and a coresponding axis is something other than 0
                if (Input.GetAxis("Vertical") > 0)
                {
                    motor.Move(data.moveSpeed); //calls the Move function on the TankMotor script passing in the moveSpeed value from TankData
                }
                else if (Input.GetAxis("Vertical") < 0)
                {
                    motor.Move(-data.moveSpeed); //calls the Move function on the TankMotor script passing in the -moveSpeed value from TankData
                }
                if (Input.GetAxis("Horizontal") > 0)
                {
                    motor.Turn(data.rotationSpeed); //calls the Turn function on the TankMotor script passing in the rotationSpeed value from TankData
                }
                else if (Input.GetAxis("Horizontal") < 0)
                {
                    motor.Turn(-data.rotationSpeed); //calls the Turn function on the TankMotor script passing in the -rotationSpeed value from TankData
                }
                if (Input.GetButton("Jump"))
                {
                    if (Time.time >= nextTimeToShoot) // if the current time is greater than or equal to the nextTimeToShoot then the tank can shoot
                    {
                        motor.Shoot(data.shellPrefab, data.shotForce, data.cannonPosition); // calls the shoot function on the tankMotor script passing in the
                                                                                            // shellPrefab, force, and position to instatiate a shot
                        nextTimeToShoot = Time.time + data.reloadTime; // nextTimeToShoot equals current time + the reload time held in TankData
                    }
                }
                break;
        }
    }
}
