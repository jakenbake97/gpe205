﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RestorePreviousSettings : MonoBehaviour
{
    public Slider fxVolume; // fx volume slider
    public Slider musicVolume; // music volume slider
    public Toggle multiplayer; // multiplayer toggle
    public TMP_Dropdown player1Controls; // player controls dropdown
    public TMP_Dropdown player2Controls; // player controls dropdown
    public TMP_Dropdown mapGeneration; // map generation dropdown
    public TMP_InputField gridX; // grid x input
    public TMP_InputField gridY; // grid x input
    public TMP_InputField seedInput; // seed input
    public TMP_Dropdown difficulty; // difficulty dropdown

    // Use this for initialization
    void Start()
    {
        fxVolume.value = PlayerPrefs.GetFloat("FXVolume", 0f); // the value of the fx slider equals the one we have saved
        musicVolume.value = PlayerPrefs.GetFloat("MusicVolume", 0f); // the value of the music slider equals the one we have saved
        if (PlayerPrefs.GetString("Multiplayer", "false") == "True") // if multiplayer was last set to true
        {
            multiplayer.isOn = true; // set it to true this time
        }
        else
        {
            multiplayer.isOn = false; // otherwise it should be false
        }
        player1Controls.value = PlayerPrefs.GetInt("Player1Controls", 3); // the value of the controls dropdown we have saved
        player2Controls.value = PlayerPrefs.GetInt("Player2Controls", 0); // the value of the controls dropdown we have saved
        mapGeneration.value = PlayerPrefs.GetInt("MapGeneration", 1); // the value of the generation dropdown we have saved
        gridX.text = PlayerPrefs.GetString("GridX", " "); // set the grid x to equal whatever we have saved
        gridY.text = PlayerPrefs.GetString("GridY", " "); // set the grid y to equal whatever we have saved
        seedInput.text = PlayerPrefs.GetString("SeedInput", " "); // set the seed equal to whatever we have saved
        difficulty.value = PlayerPrefs.GetInt("Difficulty", 0); // set the difficulty to be whatever we have saved
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetOptions() // when the player exits the options menu save all of the values
    {
        PlayerPrefs.SetFloat("FXVolume", fxVolume.value);
        PlayerPrefs.SetFloat("MusicVolume", musicVolume.value);
        PlayerPrefs.SetString("Multiplayer", multiplayer.isOn.ToString());
        PlayerPrefs.SetInt("Player1Controls", player1Controls.value);
        PlayerPrefs.SetInt("Player2Controls", player2Controls.value);
        PlayerPrefs.SetInt("MapGeneration", mapGeneration.value);
        PlayerPrefs.SetString("GridX", gridX.text);
        PlayerPrefs.SetString("GridY", gridY.text);
        PlayerPrefs.SetString("SeedInput", seedInput.text);
        PlayerPrefs.SetInt("Difficulty", difficulty.value);
    }
}
