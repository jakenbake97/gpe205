﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

[System.Serializable]
public class ScoreboardController : MonoBehaviour
{
    GameManager gmRef; // reference to game manager
    // the text items for names of leaderboard
    public TMP_Text boardName1;
    public TMP_Text boardName2;
    public TMP_Text boardName3;
    public TMP_Text boardName4;
    public TMP_Text boardName5;
    // the text items for scores of leaderboard
    public TMP_Text boardScore1;
    public TMP_Text boardScore2;
    public TMP_Text boardScore3;
    public TMP_Text boardScore4;
    public TMP_Text boardScore5;
    // the text items for the player scores this game
    public TMP_Text Player1Score;
    public TMP_Text Player2Score;

    public struct NameAndScore // struct to create new type that holds both a name and score
    {
        public string name;
        public float score;
    }
    // NameAndScore type which holds both name and score
    NameAndScore score1;
    NameAndScore score2;
    NameAndScore score3;
    NameAndScore score4;
    NameAndScore score5;


    List<NameAndScore> scores; // a list of names and score which can be sorted and stored
    // Use this for initialization
    void Start()
    {
        scores = new List<NameAndScore>(); // initilaize the list
        gmRef = GameManager.instance;
        // set the NameAndScore variables to hold the 5 names and scores that we have saved
        score1.name = PlayerPrefs.GetString("Score1S", "N/A");
        score1.score = PlayerPrefs.GetFloat("Score1", 0f);
        score2.name = PlayerPrefs.GetString("Score2S", "N/A");
        score2.score = PlayerPrefs.GetFloat("Score2", 0f);
        score3.name = PlayerPrefs.GetString("Score3S", "N/A");
        score3.score = PlayerPrefs.GetFloat("Score3", 0f);
        score4.name = PlayerPrefs.GetString("Score4S", "N/A");
        score4.score = PlayerPrefs.GetFloat("Score4", 0f);
        score5.name = PlayerPrefs.GetString("Score5S", "N/A");
        score5.score = PlayerPrefs.GetFloat("Score5", 0f);

        // add all of those combos to our list
        scores.Add(score1);
        scores.Add(score2);
        scores.Add(score3);
        scores.Add(score4);
        scores.Add(score5);

        // set the leaderboard to display the names and scores
        boardName1.text = score1.name;
        boardName2.text = score2.name;
        boardName3.text = score3.name;
        boardName4.text = score4.name;
        boardName5.text = score5.name;
        boardScore1.text = score1.score.ToString();
        boardScore2.text = score2.score.ToString();
        boardScore3.text = score3.score.ToString();
        boardScore4.text = score4.score.ToString();
        boardScore5.text = score5.score.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (gmRef.scores.Length > 1) // if we have multiple players
        {
            Player1Score.text = gmRef.scores[0].ToString(); // set the score to show player 1
            Player2Score.text = gmRef.scores[1].ToString(); // set the score to show player 2
        }
        else // otherwise
        {
            Player1Score.text = gmRef.scores[0].ToString(); // just show player 1's score
        }


    }

    public void SetScoresP1(string name) // sets the scores for player 1 when a name is entered
    {
        NameAndScore player1;
        player1.name = name; // name equals name input
        player1.score = gmRef.scores[0]; // score equals the players score
        scores.Add(player1); // add it to the list
        SortAndUpdateHighScores(); // call our sort and update function
    }

    public void SetScoresP2(string name) // sets the scores for player 2 when name is entered
    {
        NameAndScore player2;
        player2.name = name; // name equals name input
        player2.score = gmRef.scores[1]; // score equals the player's score
        scores.Add(player2); // add it to the list
        SortAndUpdateHighScores(); // call our sort and update function
    }

    void SortAndUpdateHighScores()
    {
        scores.Sort(delegate (NameAndScore scoreA, NameAndScore scoreB) // sorts the list and compares the scores
        {
            return scoreA.score.CompareTo(scoreB.score);
        });
        scores.Reverse(); // returns the list in a highest to lowest order

        // store the top 5 names and scores
        PlayerPrefs.SetString("Score1S", scores[0].name);
        PlayerPrefs.SetString("Score2S", scores[1].name);
        PlayerPrefs.SetString("Score3S", scores[2].name);
        PlayerPrefs.SetString("Score4S", scores[3].name);
        PlayerPrefs.SetString("Score5S", scores[4].name);
        PlayerPrefs.SetFloat("Score1", scores[0].score);
        PlayerPrefs.SetFloat("Score2", scores[1].score);
        PlayerPrefs.SetFloat("Score3", scores[2].score);
        PlayerPrefs.SetFloat("Score4", scores[3].score);
        PlayerPrefs.SetFloat("Score5", scores[4].score);


        // update the leaderboard to show the new top 5 names and scores
        boardName1.text = scores[0].name;
        boardName2.text = scores[1].name;
        boardName3.text = scores[2].name;
        boardName4.text = scores[3].name;
        boardName5.text = scores[4].name;
        boardScore1.text = scores[0].score.ToString();
        boardScore2.text = scores[1].score.ToString();
        boardScore3.text = scores[2].score.ToString();
        boardScore4.text = scores[3].score.ToString();
        boardScore5.text = scores[4].score.ToString();

    }
}
