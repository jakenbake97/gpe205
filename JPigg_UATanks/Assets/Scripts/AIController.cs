﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankData))] // Require TankData component to be on the gameobject

[RequireComponent(typeof(TankMotor))] // Require TankMotor component to be on the gameobject 
public class AIController : MonoBehaviour
{
    enum AIState { Patrol, Chase, Attack, Flee, Idle }; // The state for each unit type
    AIState aIState = AIState.Patrol; // Defaults to Patrol (chase if tactician)
    TankMotor motor; // holds a reference to the TankMotor script on this gameobject
    TankData data; // holds a reference to the TankData script on this gameobject
    Transform tankTransform; // holds a reference to this gameobject's transform
    float nextTimeToShoot; // the next time the tank will be able to shoot, based on reload time in TankData.
    Transform moveTarget; // Used by the Protector to set the nearest tank to move to
    Transform playerTarget; // the player's transform when it is being targeted
    int avoidanceStage = 0; // 0, 1, or 2. stages avoiding obstacles
    bool playerSeen = false; // boolean to determine if the player was seen this frame
    float closeEnough = 1.0f; // the max distance away from a patrolpoint a tank can be for it to count
    int currentWaypoint = 0; // The waypoint a tank is currently moving towards
    float avoidanceTime = 2.0f; // The amount of time "avoidance mode" will be in effect
    float exitTime; // Holds the time that avoidance mode can be exited (game time + avoidance time)

    void Start()
    {
        motor = gameObject.GetComponent<TankMotor>(); // sets the motor equal to the TankMotor component
        data = gameObject.GetComponent<TankData>(); // sets the data variable equal to the TankData component
        nextTimeToShoot = Time.time; // sets the starting time to shoot equal to the time when starting, so the tank can shoot right away
        tankTransform = gameObject.GetComponent<Transform>(); // Sets this tank's transform equal to tankTransform to prevent getComponent each time
        if (data.personality == GameManager.Personality.Tactician) // Sets the default state to Chase for the Tactician personality
        {
            ChangeState(AIState.Chase);
        }

    }

    // Update is called once per frame
    void Update()
    {
        foreach (Transform player in data.playerPositions) // cycles through the playerpositions that we have to see if they are null
        {
            if (player != null) // if the player position isnt null
            {
                DeterminePersonality(); // then determine personality
                break; // we can break the loop if one player is alive
            }

        }

    }

    private void DeterminePersonality()
    {
        // switches through all possible personality types to find the correct one for this tank
        switch (data.personality)
        {
            case GameManager.Personality.Protector:
                foreach (Transform player in data.playerPositions) // for each tank in the playerPositions array
                {
                    if (player != null) // prevents us from attempting to use a null transform
                    {
                        playerSeen = CanSee(player.gameObject, data.fieldOfView); // determine if the player can be seen
                        if (Vector3.SqrMagnitude(player.position - tankTransform.position) <= (data.attackRange * data.attackRange)) // if the distance to the player is less
                                                                                                                                     // than the attack range, target the player
                        {
                            playerTarget = player; // set the player target to be the current player in the loop
                            break;
                        }
                        else
                        {
                            playerSeen = false; // otherwise the player isn't seen
                            playerTarget = null; // so our target is null
                        }
                    }
                }
                if (playerTarget != null && aIState != AIState.Attack && playerSeen) // if we have a target and we currently arent attacking
                {
                    ChangeState(AIState.Attack); // we should switch to attack
                }
                if (aIState == AIState.Attack) // if we are attacking
                {
                    if (playerTarget != null && playerSeen) // and if we still have a target
                    {
                        Attack(playerTarget); // attack the target
                    }
                    else
                    {
                        ChangeState(AIState.Chase); // otherwise switch to chase (find ally)
                    }
                }
                else if (aIState == AIState.Chase) // if we are in chase
                {
                    if (moveTarget == null) // and we don't have an ally
                    {
                        Chase(moveTarget); // call chase to find one
                        if (moveTarget == null) // if we cant find one
                        {
                            ChangeState(AIState.Idle); // then idle
                        }
                        else
                        {
                            ChangeState(AIState.Patrol); // if we do find an ally, follow them
                        }
                    }
                    else
                    {
                        ChangeState(AIState.Patrol); // if we already have an ally, follow
                    }
                }
                else if (aIState == AIState.Idle) // if we are idleing
                {
                    Idle(); // call the idle function
                }
                else if (aIState == AIState.Patrol) // if our state is patrol
                {
                    if (moveTarget == null) // and our ally is missing
                    {
                        ChangeState(AIState.Chase); // switch to chase to find one
                    }
                    else // otherwise if we have an ally
                    {
                        if (avoidanceStage != 0) // and we arent avaoiding anything
                        {
                            AvoidObstacle(data.moveSpeed);
                        }
                        else
                        {
                            Patrol(); // then patrol
                        }

                    }
                }
                break;
            case GameManager.Personality.Patroller:
                foreach (Transform player in data.playerPositions) // for each tank in the playerPositions array
                {
                    if (player != null) // prevents us from attempting to use a null transform
                    {
                        if (CanSee(player.gameObject, data.fieldOfView)) // if the player can be seen set the target to be that player
                        {
                            playerSeen = true;
                            playerTarget = player;
                            break;
                        }
                        else
                        {
                            playerSeen = false; // otherwise, we cant see the player
                        }
                    }
                }
                if (playerTarget != null && (aIState != AIState.Attack && aIState != AIState.Chase)) // if we have a target and are not currently attacking or chasing
                {
                    ChangeState(AIState.Chase); // then switch to chase
                }
                if (aIState == AIState.Attack) // if we are currently attacking
                {
                    if (playerTarget != null) // and we still have a target
                    {
                        if (Vector3.SqrMagnitude(playerTarget.position - tankTransform.position) <= (data.attackRange * data.attackRange)) // if the target is in range
                        {
                            Attack(playerTarget); // attack the target
                        }
                        else
                        {
                            ChangeState(AIState.Chase); // otherwise chase the target
                        }
                    }
                    else
                    {
                        ChangeState(AIState.Patrol); // if we don't have a target then switch back to patrol
                    }
                }
                else if (aIState == AIState.Chase) // if we are currently chasing
                {
                    if (playerTarget != null) // if have a target
                    {
                        if (Vector3.SqrMagnitude(playerTarget.position - tankTransform.position) <= (data.attackRange * data.attackRange)) // and if that target is in range
                        {
                            if (playerSeen) // and we can see that target
                            {
                                ChangeState(AIState.Attack); // then switch to attack
                            }
                        }
                        if (avoidanceStage != 0) // if the target isnt in range, check to see if we are avoiding anything
                        {
                            AvoidObstacle(data.moveSpeed); // if so, then continue avoiding
                        }
                        else
                        {
                            Chase(playerTarget); // otherwise if we arent avoiding then chase the target
                        }
                    }
                    else
                    {
                        ChangeState(AIState.Patrol); // if we no longer have a target then switch to patrol
                    }
                }
                else if (aIState == AIState.Patrol) // if we are patrolling
                {
                    if (avoidanceStage != 0) // if we are avoiding an obstacle
                    {
                        AvoidObstacle(data.moveSpeed); // then continue avoiding
                    }
                    else
                    {
                        Patrol(); // otherwise continue patrolling
                    }
                }
                break;
            case GameManager.Personality.Coward:
                foreach (Transform player in data.playerPositions) // for each tank in the playerPositions array
                {
                    if (player != null) // prevents us from attempting to use a null transform
                        if (CanSee(player.gameObject, data.fieldOfView)) // if we can the player, then we set it as our target
                        {
                            playerSeen = true;
                            playerTarget = player;
                            break;
                        }
                        else
                        {
                            playerSeen = false; // otherwise we can't see the player
                        }
                }
                if (playerTarget != null && aIState == AIState.Patrol) // if we have a target and are currently patrolling
                {
                    ChangeState(AIState.Flee); // we are going to switch to flee from the target
                }
                if (aIState == AIState.Attack) // if we are attacking
                {
                    if (playerTarget == null) // if we lose our target
                    {
                        ChangeState(AIState.Patrol); // go back to patrol
                    }
                    else // otherwise
                    {
                        // if the player is closer to us than the flee distance
                        if (Vector3.SqrMagnitude(playerTarget.position - tankTransform.position) < (data.fleeDistance * data.fleeDistance))
                        {
                            ChangeState(AIState.Flee); // we should flee again
                        }
                        else
                        {
                            Attack(playerTarget); // if the player is farther, then we will attack
                        }
                    }

                }
                else if (aIState == AIState.Flee) // if we are fleeing
                {
                    if (playerTarget != null) // and we still have a target
                    {
                        if (avoidanceStage != 0) // if are avoind an obstacle
                        {
                            AvoidObstacle(data.moveSpeed); // continue to avoid
                        }
                        else // otherwise
                        {
                            // if the target is closer to us than flee distance
                            if (Vector3.SqrMagnitude(playerTarget.position - tankTransform.position) < (data.fleeDistance * data.fleeDistance))
                            {
                                Flee(playerTarget); // continue to flee the target
                            }
                            else // if the player is farther
                            {
                                ChangeState(AIState.Attack); // we can safely switch to attack
                            }

                        }
                    }
                }
                else if (aIState == AIState.Patrol) // if we are patrolling
                {
                    if (avoidanceStage != 0) // if we are avoiding an obstacle
                    {
                        AvoidObstacle(data.moveSpeed);// continue to avoid
                    }
                    else // otherwise
                    {
                        Patrol(); // continue our patrol
                    }
                }
                break;
            case GameManager.Personality.Tactician:
                foreach (Transform player in data.playerPositions) // for each tank in the playerPositions array
                {
                    if (player != null) // prevents us from attempting to use a null transform
                    {
                        if (CanSee(player.gameObject, data.fieldOfView)) // if we can see the player then player seen = true
                        {
                            playerSeen = true;
                            playerTarget = player;
                            break;
                        }
                        else
                        {
                            playerSeen = false;
                            playerTarget = player; // even if we can't see the player, the tactician cal still target it
                        }
                    }
                }
                if (aIState == AIState.Chase) // if we are chasing the target
                {
                    if (avoidanceStage != 0) // if we need to avoid an obstacle
                    {
                        AvoidObstacle(data.moveSpeed); // then we should avoid that obstacle
                    }
                    if (data.tankHealth < data.startingHealth * 0.3f) // if our health is below 30%
                    {
                        ChangeState(AIState.Flee); // we should flee
                    }
                    // if the target is closer than our maximum range
                    if (Vector3.SqrMagnitude(playerTarget.position - tankTransform.position) <= (data.attackRange * data.attackRange) && playerSeen)
                    {
                        ChangeState(AIState.Attack); // we switch to attack the player
                    }
                    else
                    {
                        Chase(playerTarget); // otherwise we continue chasing the player
                    }
                }
                else if (aIState == AIState.Attack) // if we are attacking
                {
                    if (avoidanceStage != 0) // if we need to avoid an obstacle
                    {
                        AvoidObstacle(data.moveSpeed); // then we should avoid that obstacle
                    }
                    if (data.tankHealth < data.startingHealth * 0.3f) // if our health is below 30%
                    {
                        ChangeState(AIState.Flee); // we switch to flee
                    }
                    // if the target is closer than our maximum range
                    if (Vector3.SqrMagnitude(playerTarget.position - tankTransform.position) <= (data.attackRange * data.attackRange) && playerSeen)
                    {
                        Attack(playerTarget); // we can attack the target
                    }
                    else
                    {
                        ChangeState(AIState.Chase); // otherwise we switch back to chase the target
                    }
                }
                else if (aIState == AIState.Flee) // if we are fleeing
                {
                    if (avoidanceStage != 0) // if we need to avoid an obstacle
                    {
                        AvoidObstacle(data.moveSpeed); // avoid the obstacle
                    }
                    // if the target is closer than our flee distance
                    if (Vector3.SqrMagnitude(playerTarget.position - tankTransform.position) < (data.fleeDistance * data.fleeDistance))
                    {
                        Flee(playerTarget); // we continue fleeing
                    }
                    else
                    {
                        ChangeState(AIState.Idle); // otherwise, we idle to heal
                    }
                }
                else if (aIState == AIState.Idle) // if we are idleing
                {
                    // if the target is closer than our flee distance
                    if (Vector3.SqrMagnitude(playerTarget.position - tankTransform.position) < (data.fleeDistance * data.fleeDistance))
                    {
                        ChangeState(AIState.Flee); // then we stop healing and flee
                    }
                    else
                    {
                        Idle(); // otherwise we idle to heal
                    }
                }
                break;
            default:
                Debug.LogError("ERROR: AI Personality Not Set"); // in case there somehow isnt a personality selected
                break;
        }
    }

    void Patrol() // patrol through waypoints
    {
        if (data.personality == GameManager.Personality.Protector) // if we are a protector
        {
            if (!motor.RotateTowards(moveTarget.position, data.rotationSpeed)) // we rotate toward our ally
            {
                if (CanMove(data.moveSpeed)) // if we can move,
                {
                    motor.Move(data.moveSpeed); // we do so.
                }
                else
                {
                    avoidanceStage = 1; // otherwise we start avoiding the obstacle
                }

            }
        }
        else if (data.personality == GameManager.Personality.Patroller || data.personality == GameManager.Personality.Coward) // if we are a patroller or coward
        {
            if (!motor.RotateTowards(data.patrolPoints[currentWaypoint].position, data.rotationSpeed)) // we rotate toward our ally
            {
                if (CanMove(data.moveSpeed)) // if we can move
                {
                    motor.Move(data.moveSpeed); // we do so.
                }
                else
                {
                    avoidanceStage = 1; // otherwise we start avoiding the obstacle
                }
            }
            // if we are close enough to the waypoint
            if (Vector3.SqrMagnitude(data.patrolPoints[currentWaypoint].position - tankTransform.position) <= closeEnough)
            {
                if (currentWaypoint != data.patrolPoints.Length - 1)
                {
                    currentWaypoint++; // we go to the next waypoint
                }
                else
                {
                    currentWaypoint = 0; // if we are at the last waypoint, we start over
                }
            }
        }
    }

    void Chase(Transform target) // chases a target (or finds an ally)
    {
        if (data.personality == GameManager.Personality.Protector) // if we are the protector
        {
            GameObject[] allies = GameObject.FindGameObjectsWithTag("Tank"); // we loof for all of our allies
            if (allies.Length > 1) // if we have any
            {
                float closest = Mathf.Infinity;
                foreach (GameObject ally in allies) // we check to see which is closest
                {
                    if (ally != this.gameObject)
                    {
                        float distance = Vector3.SqrMagnitude(ally.transform.position - tankTransform.position);
                        if (distance < closest)
                        {
                            closest = distance;
                            moveTarget = ally.GetComponent<Transform>(); // the closest becomes our target
                        }
                    }
                }
            }
            else // if we dont have any allies
            {
                moveTarget = null; // the target is null
            }
        }
        else if (data.personality == GameManager.Personality.Patroller || data.personality == GameManager.Personality.Tactician) // if we are a patroller or tactician
        {
            motor.RotateTowards(target.position, data.rotationSpeed); // we rotate towards the target
            if (CanMove(data.moveSpeed)) // if we can move
            {
                motor.Move(data.moveSpeed); // we do so
            }
            else
            {
                avoidanceStage = 1; // otherwise, we start obstacle avoidance
                AvoidObstacle(data.moveSpeed);
            }
        }
    }

    void Attack(Transform target) // attacks a target if it can be seen
    {
        if (data.personality == GameManager.Personality.Protector || data.personality == GameManager.Personality.Coward) // if we are the protector or coward
        {
            motor.RotateTowards(target.position, data.rotationSpeed); // we rotate towards out target, but dont move towards them
            if (playerSeen) // if we can see the player
            {
                if (Time.time > nextTimeToShoot) // and if its time to shoot
                {
                    motor.Shoot(data.shellPrefab, data.shotForce, data.cannonPosition); // we shoot at the player
                    nextTimeToShoot = Time.time + data.reloadTime; // and schedule our next shot
                }
            }

        }
        else // if we are any other unit type
        {
            motor.RotateTowards(target.position, data.rotationSpeed); // we rotate towards our target
            if (CanMove(data.moveSpeed)) // and if we can move
            {
                motor.Move(data.moveSpeed); // we do so
            }
            else // otherwise
            {
                avoidanceStage = 1; // we begin obstacle avoidance
            }
            if (playerSeen) // if we see the player
            {
                if (Time.time > nextTimeToShoot) // and if its time to shoot
                {
                    motor.Shoot(data.shellPrefab, data.shotForce, data.cannonPosition); // we shoot at the player
                    nextTimeToShoot = Time.time + data.reloadTime; // and schedule our next shot
                }
            }
            else // if we can't see the player, we switch back to chase
            {
                ChangeState(AIState.Chase);
            }

        }

    }

    void Flee(Transform target) // flees from a target
    {
        Vector3 vectorToTarget = target.position - tankTransform.position; // get teh vector to our target, by taking their position minus ours

        Vector3 vectorFromTarget = -1 * vectorToTarget; // we then flip that vector

        vectorFromTarget.Normalize(); // and normalize it to 1

        vectorFromTarget *= data.fleeDistance; // then set that to be multiplied by our flee distance

        Vector3 fleePosition = vectorFromTarget + tankTransform.position; // our flee position is created from the vector plus our position
        motor.RotateTowards(fleePosition, data.rotationSpeed); // rotate to look at the flee position
        if (CanMove(data.moveSpeed)) // if we can move
        {
            motor.Move(data.moveSpeed); // do so
        }
        else
        {
            avoidanceStage = 1; // otherwise, avoid
        }

    }
    void Idle() // idles and heals 
    {
        if (data.personality == GameManager.Personality.Protector) // if we are the protector
        {
            motor.Turn(data.rotationSpeed); // we spin while we search for the player
        }
        else if (data.personality == GameManager.Personality.Tactician) // if we are the tactician
        {
            if (data.tankHealth < data.startingHealth) // and if our health is less than the starting health
            {
                data.tankHealth += 5 * Time.deltaTime; // we should heal
            }
            else
            {
                ChangeState(AIState.Chase); // otherwise we need to go back to chasing the player
            }

        }
    }

    void AvoidObstacle(float moveDistance) // avoids obstacles and determines which way to turn
    {
        if (avoidanceStage == 1) // if we are in stage 1
        {
            Vector3 right45 = Quaternion.Euler(0, 45, 0) * tankTransform.forward; // get a 45 degree angle to the right
            Vector3 left45 = Quaternion.Euler(0, -45, 0) * tankTransform.forward; // and a 45 degree ablge to the left
            RaycastHit hitRight; // create 2 raycast hits
            RaycastHit hitLeft;
            Physics.Raycast(tankTransform.position, right45, out hitRight, moveDistance * 3); // and then send out the raycast using our angle to the right
            Physics.Raycast(tankTransform.position, left45, out hitLeft, moveDistance * 3); // and the angle to the left
            // if the rightside is closer to the wall than the left
            if (Vector3.SqrMagnitude(hitRight.point - tankTransform.position) <= Vector3.SqrMagnitude(hitLeft.point - tankTransform.position))
            {
                // rotate left
                motor.Turn(-1 * data.rotationSpeed); // we rotate left
            }
            else
            {
                // rotate right
                motor.Turn(data.rotationSpeed); // otherwise we rotate right
            }
            if (CanMove(moveDistance)) // if we can move
            {
                avoidanceStage = 2; // we go to stage 2 and set our time to exit avoidance
                exitTime = avoidanceTime;
            }

        }
        else if (avoidanceStage == 2) // if we are in stage 2
        {
            if (CanMove(moveDistance)) // if we can move
            {
                exitTime -= Time.deltaTime; // we subtract the time the frame took
                motor.Move(data.moveSpeed); // and move

                if (exitTime <= 0) // if we are out of time
                {
                    avoidanceStage = 0; // we go to stage 0
                }
            }
            else // if we cant move
            {
                avoidanceStage = 1; // back to stage 1
            }
        }

    }

    bool CanMove(float moveDistance) // determines if we can move forward
    {
        RaycastHit hit; // create a raycasthit
        if (Physics.Raycast(tankTransform.position, tankTransform.forward, out hit, moveDistance)) // if the raycast hit something
        {
            if (!hit.collider.CompareTag("Player") || !hit.collider.CompareTag("Tank")) // and it wasnt the player or an ally tank
            {
                return false; // return false saying we cant move
            }
        }

        return true; // otherwise return true if the path is clear
    }

    void ChangeState(AIState newState) // changes the state 
    {
        aIState = newState; // the current state equals the passed state
    }

    private void OnTriggerEnter(Collider other) // when something enters our trigger
    {
        if (other.tag == "Player") // we see if its the player
        {
            foreach (Transform player in data.playerPositions) // then cycle through our reference of players
            {
                if (other.gameObject.transform == player) // to see which one the player that entered was
                {
                    playerTarget = player; // when we find it, that player becomes our target
                }
            }
        }
    }
    private void OnTriggerExit(Collider other) // called when something exits our trigger
    {
        if (other.tag == "Player") // if it was the player
        {
            playerTarget = null; // our target becomes null since we can no longer hear it
        }
    }

    bool CanSee(GameObject target, float fieldOfView) // checks to see if we can see the player
    {
        Vector3 targetPosition = target.transform.position; // the position of the target we are trying to see
        Vector3 tankToTargetVector = targetPosition - tankTransform.position; // create a vector to that position

        float angleToTarget = Vector3.Angle(tankToTargetVector, tankTransform.forward); // find the angle to that vector
        if (angleToTarget < (fieldOfView / 2)) // if the angle is less than our feild of view / 2
        {
            Ray rayToTarget = new Ray(); // we creat a new ray

            rayToTarget.origin = tankTransform.position; // and make it originate at our position
            rayToTarget.direction = tankToTargetVector; // and set its direction to be our target vector

            RaycastHit hit; // create a raycast hit
            if (Physics.Raycast(rayToTarget, out hit, Mathf.Infinity)) // if our raycast hits something
            {
                if (hit.collider.gameObject == target) // if that something is our player
                {
                    return true; // then we can see them
                }
            }
        }
        return false; // otherwise something is blocking our view
    }
}
