﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance; // Holds an instance of this script
    public Camera mainCamera;
    public Cinemachine.CinemachineVirtualCamera[] cineCamera;
    public GameObject shellPrefab; // Holds the prefab for the shell the tanks will fire
    public float shellTimeout = 3f; // sets the time in seconds of the shellTimeout in TankData
    [Header("Player Tanks")]
    public Player playerAtributes; // reference to the player script which allows a dropdown of variables
    public GameObject playerTankPrefab; // holds the prefab for the player tank
    public int playersToSpawn = 1; // the number of players to spawn in the map at the begining of the game
    public int numberOfLives = 3;
    [HideInInspector] public GameObject[] playerTanks; // an array created to hold the created player tanks
    [HideInInspector] public int[] playerControls; // stores the controls each player is using

    public enum Personality { Protector, Patroller, Coward, Tactician }; // an enum used to allow designers to set a tank's personality

    public struct TanksWithPersonality // struct used to create a new type, allowing an enemy tank array to hold both gameobject and personality
    {
        public GameObject tankObject; // holds the gameobject of each tank
        public Personality tankPersonality; // holds the personality of each tank
    }
    TanksWithPersonality[] enemyTanks; // an array of enemy tanks after they have their personalities
    [Space(10)]
    [Header("Enemy Tanks")]
    public int difficulty = 1; // difficulty level for the game
    public int difficultyModifier = 5; // how many tanks should be spawned for each level of difficulty
    int tanksToSpawn = 4; // the number of enemy tanks the level should spawn
    public GameObject enemyTankPrefab; // the prefab for enemy tanks in the level

    [Space(3)]
    public Protector protectorAtributes; // a reference to the protector variables

    [Space(3)]
    public Patroller patrollerAtributes; // a reference to the patroller variables
    [Space(3)]
    public Coward cowardAtributes; // a reference to the coward variables

    [Space(3)]
    public Tactician tacticianAtributes; // a reference to the tactician variables

    [Space(10)]
    [Header("Map Generation")]
    public int rows; // number of rows a spawned map should have
    public int cols; // number of columns a spawned map should have
    public GameObject[] gridPrefabs; // the prefabs for different rooms in the game
    public enum GenerationType { Random, Seeded, MapOfDay }; // the type of generation to perform
    public GenerationType genType;
    public int mapSeed; // the seed to procedurally generate the map
    public float roomWidth = 50.0f; // the width of a room prefab
    public float roomHeight = 50.0f; // the height (length) of a room prefab
    private Room[,] grid; // stores all of the rooms and their locations

    [Space(10)]
    [Header("Power Ups")]
    public GameObject[] powerUpPrefabs; // the prefabs for which powerup to spawn

    [Space(10)]
    [Header("UI Components")]
    public TMP_Text[] scoresText; // references to the ui scores
    public TMP_Text[] livesText; // references to the ui lives
    public GameObject menu; // reference to the menu
    TankData[] playerTankData; // seperately stores player tank data to display scores
    public float[] scores; // stores the scores of each player so that death doesn't reset them
    int[] playerLives; // stores the number of lives each player has left

    private bool gameStarted = false; // determines if we are in menu or game mode
    // Use this for initialization
    void Awake()
    {
        if (instance == null) // Checks to see if an instance is already assigned to the variable
        {
            instance = this; // if the instance variable is null, then the instance equals this script
        }
        else
        {
            Debug.LogError("ERROR: There can only be one GameManager"); // if an instance already exists then log an error
            Destroy(gameObject); // and destroy this gameobject
        }
        playerControls = new int[2] { 3, 0 }; // sets the player controls to default values
    }

    void Update()
    {
        if (gameStarted) // if we are in game mode
        {
            int deadEnemies = 0; // set our dead enemies to 0
            SpawnPlayer(new Vector2Int(UnityEngine.Random.Range(0, cols), UnityEngine.Random.Range(0, rows))); // call the spawn player function each frame and pass in a random location
            foreach (TanksWithPersonality enemy in enemyTanks) // check if each enemy is still alive
            {
                if (enemy.tankObject == null) // if we have an empty space
                {
                    deadEnemies++; // that means an enemy died 
                }
            }
            if (deadEnemies >= tanksToSpawn) // if there are no living tanks left
            {
                GameOver(); // call the game over function
            }

        }

    }

    public void StartGame() // starts the game when the start button in the main menu is called
    {
        tanksToSpawn = difficulty * difficultyModifier; // the number of tanks to spawn based on difficulty
        enemyTanks = new TanksWithPersonality[tanksToSpawn]; // an array of enemytanks
        playerTanks = new GameObject[playersToSpawn]; // an array of playertanks
        playerTankData = new TankData[playersToSpawn]; // an array of tankData for player tanks
        scores = new float[playersToSpawn]; // stores the score for each player
        playerLives = new int[playersToSpawn]; // stores the lives of each player
        for (int i = 0; i < playersToSpawn; i++)
        {
            playerLives[i] = numberOfLives; // sets the starting lives equal to that specified by the designer
        }
        GenerateGrid(); // call the generate grid function to create our map
        SpawnPlayer(Vector2Int.zero); // spawn the player at grid 0,0
        SpawnEnemyTanks(); // spawn the enemy tanks throughout the map
        if (playersToSpawn == 1) // if single player
        {
            mainCamera.rect = new Rect(0f, 0f, 1f, 1f); // we can have a full screen camera
            cineCamera[0].GetCinemachineComponent<Cinemachine.CinemachineFramingTransposer>().m_CameraDistance = 20f;
        }
        else
        {
            mainCamera.rect = new Rect(0f, 0.5f, 1f, 1f); // otherwise the camera should just take up the top half
        }
        gameStarted = true; // we have started the game
    }

    private void SpawnPlayer(Vector2Int spawnGrid)
    {
        int numberDead = 0; // determines how many players have run out of lives
        for (int i = 0; i < playersToSpawn; i++) // cycle through to check on each player
        {
            if (playerTanks[i] == null && playerLives[i] != 0) // if any of the tanks are destroyed
            {
                GameObject spawnedTank = Instantiate(playerTankPrefab, grid[spawnGrid.x, spawnGrid.y].playerSpawn[i].position, Quaternion.identity); // instantiate a new one
                playerTanks[i] = spawnedTank; // set the slot in the array to be the newly spawned tank
                playerTankData[i] = playerTanks[i].GetComponent<TankData>(); // get a reference to that tank's tankdata component
                cineCamera[i].m_Follow = playerTanks[i].transform; // set the camera to follow the new player
                playerLives[i]--; // each time a player is spawned remove a life
                livesText[i].text = "Lives: " + playerLives[i];
                ApplyStatsToPlayerTankData(i); // apply the stats for the new player
            }
            else if (playerTanks[i] == null && playerLives[i] == 0) // if the player is dead and they don't have any lives remaining
            {
                numberDead++; // calculate tehm as dead
            }

            if (playerTankData[i].score < scores[i]) // if the score in tankdata is less than the score here
            {
                scores[i] += playerTankData[i].score; // then the player died and this is the new tanks score, so add it to the existing score
            }
            else // otherwise
            {
                scores[i] = playerTankData[i].score; // our score value will equal the ine the tank is keeping track of
            }
            scoresText[i].text = "Score: " + scores[i]; // sets the score of the coresponding text index
        }
        if (numberDead >= playersToSpawn) // if all players are dead
        {
            GameOver(); // call gameover
        }

    }

    private void ApplyStatsToPlayerTankData(int tankIndex) // apply the player's stats
    {
        playerTankData[tankIndex].tankHealth = playerAtributes.playerHealth; // set the tankHealth of tankData equal to playerHealth
        playerTankData[tankIndex].moveSpeed = playerAtributes.playerMovementSpeed; // set the moveSpeed of tankData equal to the playerMovementSpeed variable
        playerTankData[tankIndex].rotationSpeed = playerAtributes.playerRotationSpeed; // set the rotationSpeed of tankData equal to the playerRotationSpeed variable
        playerTankData[tankIndex].shellPrefab = shellPrefab; // set the shellPrefab of tankData equal to that of the GameManager ShellPrefab
        playerTankData[tankIndex].shotForce = playerAtributes.playerShotForce; // set the shotForce of tankData equal to that of playerShotForce
        playerTankData[tankIndex].reloadTime = playerAtributes.playerReloadTime; // set the reloadTime of tankData equal to the playerReloadTime
        playerTankData[tankIndex].attackDamage = playerAtributes.playerAttackDamage; // set the attackDamage of tankData equal to the playerAttackDamage
        playerTankData[tankIndex].shellTimeout = shellTimeout; // set the shellTimeout of tankData equal to shellTimeout of GameManager
        playerTankData[tankIndex].destroyScore = playerAtributes.playerDestroyScore; // set the destroyScore of tankdata equal to the playerDestroyScore
        playerTankData[tankIndex].matColor = playerAtributes.playerColor; // Sets the material color of the tankdata equal to the playerColor;
        if (playerControls[tankIndex] == 0) // based on the number, the control scheme is set
        {
            playerTanks[tankIndex].GetComponent<InputController>().inputSelection = InputController.InputScheme.arrowKeys;
        }
        else if (playerControls[tankIndex] == 1)
        {
            playerTanks[tankIndex].GetComponent<InputController>().inputSelection = InputController.InputScheme.GamePad;
        }
        else if (playerControls[tankIndex] == 2)
        {
            playerTanks[tankIndex].GetComponent<InputController>().inputSelection = InputController.InputScheme.IJKL;
        }
        else
        {
            playerTanks[tankIndex].GetComponent<InputController>().inputSelection = InputController.InputScheme.WASD;
        }
    }

    private void SpawnEnemyTanks()
    {
        int personalityToSpawn = 0; // keep track of which personality needs to be spawned
        for (int i = 0; i < tanksToSpawn; i++) // cycle through for the number of tanks to spawn
        {
            Room spawnRoom; // a reference to the room the tank spawns in
            spawnRoom = grid[UnityEngine.Random.Range(0, cols), UnityEngine.Random.Range(0, rows)]; // get a random room
            GameObject spawnedItem = Instantiate(enemyTankPrefab, spawnRoom.enemySpawn.position, Quaternion.identity); // instantiate a tank at the random location
            spawnedItem.transform.SetParent(spawnRoom.gameObject.transform); // set the parent to be the room
            TanksWithPersonality tempTank; // create a tank to hold out information until it is added to the list
            tempTank.tankObject = spawnedItem; // set the object of the tank to be the enemy we instatiated
            if (personalityToSpawn == 0) // if our personality tracker is a 0
            {
                tempTank.tankPersonality = Personality.Coward; // then this tank is going to be a coward
                cowardAtributes.cowardPoints = spawnRoom.waypoints; // so we need to get the waypoints for the room
            }
            else if (personalityToSpawn == 1) // if our personality tracker is a 1
            {
                tempTank.tankPersonality = Personality.Patroller; // then this tank is going to be a patroller
                patrollerAtributes.patrollerPoints = spawnRoom.waypoints; // sp we need to get the waypoints for the room
            }
            else if (personalityToSpawn == 2) // if our personality tracker is a 2
            {
                tempTank.tankPersonality = Personality.Protector; // then this tank is going to be a protector
            }
            else // if our personality tracker is a 3
            {
                tempTank.tankPersonality = Personality.Tactician; // then this tank is going to be a tactician
                personalityToSpawn = -1; // reset our personality counter
            }
            personalityToSpawn++; // increment the counter by 1 each time through
            enemyTanks[i] = tempTank; // set the enemy tanks array to hold this tank and it's personality
        }
        ApplyStatsToEnemyTankData(); // give all of the tanks their stats
    }

    private void ApplyStatsToEnemyTankData()
    {

        // do same foreach loop, but with enemy tanks
        foreach (TanksWithPersonality tank in enemyTanks) // cycle through each object in the enemytanks array
        {
            TankData tankData = tank.tankObject.GetComponent<TankData>(); // get a reference to that tank's tankdata component
                                                                          //Determine the personality of the specific tank and then assign the appropriate variables
            if (tank.tankPersonality == Personality.Protector)
            {
                tankData.tankHealth = protectorAtributes.protectorHealth; // set the tankHealth of tankData equal to protectorHealth
                tankData.moveSpeed = protectorAtributes.protectorMovementSpeed; // set the moveSpeed of tankData equal to the protectorMovementSpeed
                tankData.rotationSpeed = protectorAtributes.protectorRotationSpeed; // set the rotationSpeed of tankData equal to the protectorRotationSpeed
                tankData.shellPrefab = shellPrefab; // set the shellPrefab of tankData equal to that of the GameManager shellPrefab
                tankData.shotForce = protectorAtributes.protectorShotForce; // set the shotForce of tankData equal to the protectorShotForce
                tankData.reloadTime = protectorAtributes.protectorReloadTime; // set the reloadTime of tankData equal to the protectorReloadTime
                tankData.attackDamage = protectorAtributes.protectorAttackDamage; // set the attackDamage of tankdata equal to the protectorAttackDamage
                tankData.shellTimeout = shellTimeout; // set the shellTimeout of tankData equal to shellTimeout of GameManager
                tankData.destroyScore = protectorAtributes.protectorDestroyScore; // set the destroyScore of tankdata equal to the protectorDestroyScore
                tankData.matColor = protectorAtributes.protectorColor; // Sets the material color of the tankdata equal to the protectorColor
                tankData.personality = tank.tankPersonality; // Sets the personality of tankdata equal to this tank's personality
                tankData.hearingRadius = protectorAtributes.protectorHearingRadius; // Sets the hearing radius of tankData equal to the protectorHearingRadius
                tankData.fieldOfView = protectorAtributes.protectorFOV; // Sets the fieldOfView equal to that of the protectorFOV
                tankData.attackRange = protectorAtributes.protectorAtkRange; // Sets the attackRange of tankData equal to the protectorATKRange; 
            }
            else if (tank.tankPersonality == Personality.Patroller)
            {
                tankData.tankHealth = patrollerAtributes.patrollerHealth; // set the tankHealth of tankData equal to patrollerHealth
                tankData.moveSpeed = patrollerAtributes.patrollerMovementSpeed; // set the moveSpeed of tankData equal to the patrollerMovementSpeed
                tankData.rotationSpeed = patrollerAtributes.patrollerRotationSpeed; // set the rotationSpeed of tankData equal to the patrollerRotationSpeed
                tankData.shellPrefab = shellPrefab; // set the shellPrefab of tankData equal to that of the GameManager shellPrefab
                tankData.shotForce = patrollerAtributes.patrollerShotForce; // set the shotForce of tankData equal to the patrollerShotForce
                tankData.reloadTime = patrollerAtributes.patrollerReloadTime; // set the reloadTime of tankData equal to the patrollerReloadTime
                tankData.attackDamage = patrollerAtributes.patrollerAttackDamage; // set the attackDamage of tankdata equal to the patrollerAttackDamage
                tankData.shellTimeout = shellTimeout; // set the shellTimeout of tankData equal to shellTimeout of GameManager
                tankData.destroyScore = patrollerAtributes.patrollerDestroyScore; // set the destroyScore of tankdata equal to the patrollerDestroyScore
                tankData.matColor = patrollerAtributes.patrollerColor; // Sets the material color of the tankdata equal to the patrollerColor
                tankData.personality = tank.tankPersonality; // Sets the personality of tankdata equal to this tank's personality
                tankData.patrolPoints = new Transform[patrollerAtributes.patrollerPoints.Length];
                tankData.patrolPoints = patrollerAtributes.patrollerPoints; // Sets the patrol points array of tankData equal to patrollerPoints array
                tankData.hearingRadius = patrollerAtributes.patrollerHearingRadius; // Sets the hearing radius of tankData equal to the patrollerHearingRadius
                tankData.fieldOfView = patrollerAtributes.patrollerFOV; // Sets the fieldOfView equal to that of the patrollerFOV
                tankData.attackRange = patrollerAtributes.patrollerAtkRange; // Sets the attackRange of tankData equal to the patrollerATKRange;
            }
            else if (tank.tankPersonality == Personality.Coward)
            {
                tankData.tankHealth = cowardAtributes.cowardHealth; // set the tankHealth of tankData equal to cowardHealth
                tankData.moveSpeed = cowardAtributes.cowardMovementSpeed; // set the moveSpeed of tankData equal to the cowardMovementSpeed
                tankData.rotationSpeed = cowardAtributes.cowardRotationSpeed; // set the rotationSpeed of tankData equal to the cowardRotationSpeed
                tankData.shellPrefab = shellPrefab; // set the shellPrefab of tankData equal to that of the GameManager shellPrefab
                tankData.shotForce = cowardAtributes.cowardShotForce; // set the shotForce of tankData equal to the cowardShotForce
                tankData.reloadTime = cowardAtributes.cowardReloadTime; // set the reloadTime of tankData equal to the cowardReloadTime
                tankData.attackDamage = cowardAtributes.cowardAttackDamage; // set the attackDamage of tankdata equal to the cowardAttackDamage
                tankData.shellTimeout = shellTimeout; // set the shellTimeout of tankData equal to shellTimeout of GameManager
                tankData.destroyScore = cowardAtributes.cowardDestroyScore; // set the destroyScore of tankdata equal to the cowardDestroyScore
                tankData.matColor = cowardAtributes.cowardColor; // Sets the material color of the tankdata equal to the cowardColor
                tankData.personality = tank.tankPersonality; // Sets the personality of tankdata equal to this tank's personality
                tankData.patrolPoints = new Transform[cowardAtributes.cowardPoints.Length];
                tankData.patrolPoints = cowardAtributes.cowardPoints; // Sets the patrol points array of tankData equal to cowardPoints array
                tankData.hearingRadius = cowardAtributes.cowardHearingRadius; // Sets the hearing radius of tankData equal to the cowardHearingRadius
                tankData.fieldOfView = cowardAtributes.cowardFOV; // Sets the fieldOfView equal to that of the cowardFOV
                tankData.attackRange = cowardAtributes.cowardAtkRange; // Sets the attackRange of tankData equal to the cowardATKRange;
                tankData.fleeDistance = cowardAtributes.cowardFleeDistance; // Sets the fleeDistance of tankData equal to the cowardFleeDistance;
            }
            else if (tank.tankPersonality == Personality.Tactician)
            {
                tankData.tankHealth = tacticianAtributes.tacticianHealth; // set the tankHealth of tankData equal to tacticianHealth
                tankData.moveSpeed = tacticianAtributes.tacticianMovementSpeed; // set the moveSpeed of tankData equal to the tacticianMovementSpeed
                tankData.rotationSpeed = tacticianAtributes.tacticianRotationSpeed; // set the rotationSpeed of tankData equal to the tacticianRotationSpeed
                tankData.shellPrefab = shellPrefab; // set the shellPrefab of tankData equal to that of the GameManager shellPrefab
                tankData.shotForce = tacticianAtributes.tacticianShotForce; // set the shotForce of tankData equal to the tacticianShotForce
                tankData.reloadTime = tacticianAtributes.tacticianReloadTime; // set the reloadTime of tankData equal to the tacticianReloadTime
                tankData.attackDamage = tacticianAtributes.tacticianAttackDamage; // set the attackDamage of tankdata equal to the tacticianAttackDamage
                tankData.shellTimeout = shellTimeout; // set the shellTimeout of tankData equal to shellTimeout of GameManager
                tankData.destroyScore = tacticianAtributes.tacticianDestroyScore; // set the destroyScore of tankdata equal to the tacticianDestroyScore
                tankData.matColor = tacticianAtributes.tacticianColor; // Sets the material color of the tankdata equal to the tacticianColor
                tankData.personality = tank.tankPersonality; // Sets the personality of tankdata equal to this tank's personality
                tankData.fieldOfView = tacticianAtributes.tacticianFOV; // Sets the fieldOfView equal to that of the tacticianFOV
                tankData.attackRange = tacticianAtributes.tacticianAtkRange; // Sets the attackRange of tankData equal to the tacticianATKRange;
                tankData.fleeDistance = tacticianAtributes.tacticianFleeDistance; // Sets the fleeDistance of tankData equal to the tacticianFleeDistance;
            }

        }
    }

    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)]; // get a random room to spawn
    }

    public void GenerateGrid()
    {
        if (genType == GenerationType.MapOfDay) // if we are using map of the day
        {
            UnityEngine.Random.InitState(DateToInt(DateTime.Now.Date)); // set the seed to be the date
        }
        else if (genType == GenerationType.Seeded) // if we are using a seeded map
        {
            UnityEngine.Random.InitState(mapSeed); // set the seed to be the user defined seed
        }
        else // if we are using random
        {
            UnityEngine.Random.InitState(DateToInt(DateTime.Now)); // set the seed to be the time and date right now
        }
        grid = new Room[cols, rows]; // initilalize our grid for columns and rows
        for (int i = 0; i < rows; i++) // cycle through all the rows
        {
            //for each column in that row
            for (int j = 0; j < cols; j++) // cycle through all the columns
            {
                float xPosition = roomWidth * j; // the x posiiton to spawn the room equals the current column times the width of the prefab
                float zPosition = roomHeight * i; // the z position to spawn the room equals the current row times the height of the prefab
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition); // create a position bector to store the x and z positions

                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity); // instatiate a random room

                tempRoomObj.transform.parent = this.transform; // make all the rooms children of the game manager

                tempRoomObj.name = "Room_" + j + "," + i; // set the naming convention of each room
                Room tempRoomRoom = tempRoomObj.GetComponent<Room>(); // get the room component

                if (i == 0) // if this is the bottom row
                {
                    tempRoomRoom.doorNorth.SetActive(false); // remove the north door
                }
                else if (i == rows - 1) // if this is the top row
                {
                    tempRoomRoom.doorSouth.SetActive(false); // remove the south door
                }
                else // if this is any other row
                {
                    tempRoomRoom.doorNorth.SetActive(false); //remove north
                    tempRoomRoom.doorSouth.SetActive(false); // and south doors
                }

                if (j == 0) // if this is the left most column
                {
                    tempRoomRoom.doorEast.SetActive(false); // remove east door
                }
                else if (j == cols - 1) // if this is the right most column
                {
                    tempRoomRoom.doorWest.SetActive(false); // remove the west door
                }
                else // if this is any other column
                {
                    tempRoomRoom.doorEast.SetActive(false); // remove the east
                    tempRoomRoom.doorWest.SetActive(false); // and west doors
                }
                int pickupNumber = UnityEngine.Random.Range(0, powerUpPrefabs.Length); // generate a random number to pick a powerup to spawn
                tempRoomRoom.pickupPrefab = powerUpPrefabs[pickupNumber]; // set the prefab for the room to be that specific powerup
                grid[j, i] = tempRoomRoom; // set the room to the grid
            }
        }
    }

    public int DateToInt(DateTime dateToUse) // convert date and times to int
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond; // add up the date and time to get an int
    }

    public void GameOver()
    {
        menu.SetActive(true); // set the menu active
        foreach (TMP_Text text in livesText) // disable all UI
        {
            text.enabled = false;
        }
        foreach (TMP_Text text in scoresText)
        {
            text.enabled = false;
        }
    }
}

