﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Pickup : MonoBehaviour
{
    public Powerup powerup; // Reference to the powerup class
    public AudioClip feedback; // allows an audio clip to play when an item is picked up
    public AudioMixerGroup mixerGroup;
    // Use this for initialization
    public void OnTriggerEnter(Collider other)
    {
        PowerupController powCon = other.GetComponent<PowerupController>(); // gets the powerupcontroller from the other object in the collision

        if (powCon != null) // if the other object had a powerup controller
        {
            powCon.Add(powerup); // add this powerup to that controller

            if (feedback != null) // if we have an audio clip
            {
                GameObject oneShotSound = new GameObject("One Shot Sound"); // crate an empty gameobject to hold that clip
                oneShotSound.transform.position = transform.position; // set its position to be the same as this
                oneShotSound.AddComponent<AudioSource>(); // add an audio source component
                AudioSource source = oneShotSound.GetComponent<AudioSource>(); // get a reference to that component
                source.clip = feedback; // set the clip equal the one we have 
                source.outputAudioMixerGroup = mixerGroup; // set the mixer so we can control volume
                source.Play(); // play the clip
                Destroy(oneShotSound, feedback.length); // destory this gameobject after the clip
            }

            Destroy(gameObject); // destroy the gameobject so a new pickup can spawn
        }
    }
}
