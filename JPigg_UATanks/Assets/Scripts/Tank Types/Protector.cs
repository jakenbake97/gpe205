﻿using UnityEngine;

[System.Serializable]
public class Protector
{
    public float protectorHealth = 50f; // Sets the tankHealth of the Protector enemy type
    public float protectorMovementSpeed = 3f; // Sets the movementSpeed of the Protector enemy type
    public float protectorRotationSpeed = 180f; // Sets the rotationSpeed of the Protector enemy type
    public float protectorShotForce = 15f; // Sets the shotForce for shells fired by the Protector enemy type
    public float protectorReloadTime = 2.5f; // sets the reload time in between shots on the Protector enemy type
    public float protectorAttackDamage = 20f; // sets the attackDamage of the Protector enemy type
    public int protectorDestroyScore = 50; // sets the destroyScore of the Protector enemy type
    public Color protectorColor = Color.blue; // The color that this personality's body will be
    public float protectorHearingRadius = 10f; // The radius in which the protector can hear the player
    public float protectorFOV = 90f; // the angle which out tank can see in the forward direction
    public float protectorAtkRange = 10f; // the maximum distance the tank can attack a target;

    // Note: Protector does not need patrol points since it will simply follow the nearest tank
}
