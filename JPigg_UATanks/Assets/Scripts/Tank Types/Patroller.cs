﻿using UnityEngine;

[System.Serializable]
public class Patroller
{
    public float patrollerHealth = 50f; // Sets the tankHealth of the Patroller enemy type
    public float patrollerMovementSpeed = 3f; // Sets the movementSpeed of the Patroller enemy type
    public float patrollerRotationSpeed = 180f; // Sets the rotationSpeed of the Patroller enemy type
    public float patrollerShotForce = 15f; // Sets the shotForce for shells fired by the Patroller enemy type
    public float patrollerReloadTime = 2.5f; // sets the reload time in between shots on the Patroller enemy type
    public float patrollerAttackDamage = 20f; // sets the attackDamage of the Patroller enemy type
    public int patrollerDestroyScore = 50; // sets the destroyScore of the Patroller enemy type
    [HideInInspector] public Transform[] patrollerPoints; // The patroller used patrol points to move throughout the map
    public Color patrollerColor = Color.red; // The color that this personality's body will be
    public float patrollerHearingRadius = 12f; // The radius in which the patroller can hear the player
    public float patrollerFOV = 90f; // the angle which out tank can see in the forward direction
    public float patrollerAtkRange = 10f; // the maximum distance the tank can attack a target;

}
