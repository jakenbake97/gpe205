﻿using UnityEngine;

[System.Serializable]
public class Player
{
    public float playerHealth = 100f; // Sets the tankHealth of all players
    public float playerMovementSpeed = 3f; // Sets the movementSpeed of all player tanks
    public float playerRotationSpeed = 180f; // Sets the rotationSpeed of all player tanks
    public float playerShotForce = 15f; // Sets the shotForce for shells fired by the player
    public float playerReloadTime = 2.5f; // Sets the reload time in between shots on player tanks
    public float playerAttackDamage = 20f; // Sets the attackDamage of the player tanks
    public int playerDestroyScore = 100; // sets the destroyScore of playertanks for multiplayer
    public Color playerColor = Color.green; // sets the color of the player tank
}
