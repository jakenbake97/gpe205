﻿using UnityEngine;

[System.Serializable]
public class Coward
{

    public float cowardHealth = 50f; // Sets the tankHealth of the Coward enemy type
    public float cowardMovementSpeed = 3f; // Sets the movementSpeed of the Coward enemy type
    public float cowardRotationSpeed = 180f; // Sets the rotationSpeed of the Coward enemy type
    public float cowardShotForce = 15f; // Sets the shotForce for shells fired by the Coward enemy type
    public float cowardReloadTime = 2.5f; // sets the reload time in between shots on the Coward enemy type
    public float cowardAttackDamage = 20f; // sets the attackDamage of the Coward enemy type
    public int cowardDestroyScore = 50; // sets the destroyScore of the Coward enemy type
    [HideInInspector] public Transform[] cowardPoints; // The coward is the other unit to use patrol points to move throughout the map
    public Color cowardColor = Color.white; // The color that this personality's body will be
    public float cowardHearingRadius = 15f; // The radius in which the coward can hear the player
    public float cowardFOV = 90f; // the angle which out tank can see in the forward direction
    public float cowardAtkRange = 15f; // the maximum distance the tank can attack a target;
    public float cowardFleeDistance = 8f; // The minimum distance to get away from the player
}
