﻿using UnityEngine;

[System.Serializable]
public class Tactician
{
    public float tacticianHealth = 50f; // Sets the tankHealth of the Tactician enemy type
    public float tacticianMovementSpeed = 3f; // Sets the movementSpeed of the Tactician enemy type
    public float tacticianRotationSpeed = 180f; // Sets the rotationSpeed of the Tactician enemy type
    public float tacticianShotForce = 15f; // Sets the shotForce for shells fired by the Tactician enemy type
    public float tacticianReloadTime = 2.5f; // sets the reload time in between shots on the Tactician enemy type
    public float tacticianAttackDamage = 20f; // sets the attackDamage of the Tactician enemy type
    public int tacticianDestroyScore = 50; // sets the destroyScore of the Tactician enemy type
    public Color tacticianColor = Color.black; // The color that this personality's body will be
    public float tacticianFOV = 90f; // the angle which out tank can see in the forward direction. used to determine if player is in line of sight
    // Note: tactician does not need patrol points since it wil always know where the player is.
    public float tacticianAtkRange = 10f; // the maximum distance the tank can attack a target;
    public float tacticianFleeDistance = 5f; // The minimum distance to get away from the player
}
