﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAIController : MonoBehaviour
{
    public Transform[] waypoints;
    public TankData data;
    public TankMotor motor;
    public float closeEnough = 1.0f;
    public enum LoopType { Stop, Loop, PingPong };
    public LoopType loopType;

    private int currentWaypoint = 0;
    private bool isPatrolForward = true;
    Transform tf;
    void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (motor.RotateTowards(waypoints[currentWaypoint].position, data.rotationSpeed))
        {
            // Do nothing
        }
        else
        {
            // Move Forward
            motor.Move(data.moveSpeed);
        }
        // If we are close to the waypoint,
        if (Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {
            if (loopType == LoopType.Stop)
            {
                if (currentWaypoint != waypoints.Length - 1)
                {
                    // advance to the next waypoint
                    currentWaypoint++;
                }
            }

            else if (loopType == LoopType.Loop)
            {
                if (currentWaypoint != waypoints.Length - 1)
                {
                    // advance to the next waypoint
                    currentWaypoint++;
                }
                else
                {
                    currentWaypoint = 0;
                }
            }
            else if (loopType == LoopType.PingPong)
            {
                if (isPatrolForward)
                {
                    if (currentWaypoint != waypoints.Length - 1)
                    {
                        currentWaypoint++;
                    }
                    else
                    {
                        isPatrolForward = false;
                        currentWaypoint--;
                    }

                }
                else
                {
                    if (currentWaypoint != 0)
                    {
                        currentWaypoint--;
                    }
                    else
                    {
                        isPatrolForward = true;
                        currentWaypoint++;
                    }
                }
            }


        }
    }
}
