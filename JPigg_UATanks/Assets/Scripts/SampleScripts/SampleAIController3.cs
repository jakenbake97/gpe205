﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAIController3 : MonoBehaviour
{
    public Transform target;
    public float avoidanceTime = 2.0f;
    public enum AttackMode { Chase };
    public AttackMode attackMode;
    private TankData data;
    private TankMotor motor;
    private Transform tankTransform;
    private int avoidanceStage = 0;
    private float exitTime;
    // Use this for initialization
    void Start()
    {
        data = gameObject.GetComponent<TankData>();
        motor = gameObject.GetComponent<TankMotor>();
        tankTransform = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (attackMode == AttackMode.Chase)
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();
            }
        }
    }

    void DoAvoidance()
    {
        if (avoidanceStage == 1)
        {
            // Rotate left
            motor.Turn(-1 * data.rotationSpeed);

            // If I can now move forward, move to stage 2!
            if (CanMove(data.moveSpeed))
            {
                avoidanceStage = 2;

                // Set the number of seconds we will stay in Stage2
                exitTime = avoidanceTime;
            }

            // Otherwise, we'll do this again next turn!
        }
        else if (avoidanceStage == 2)
        {
            // if we can move forward, do so
            if (CanMove(data.moveSpeed))
            {
                // Subtract from our timer and move
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);

                // If we have moved lond enough, return to chase mode
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                // Otherwise, we can't move forward, so back to stage 1
                avoidanceStage = 1;
            }
        }
    }

    void DoChase()
    {
        motor.RotateTowards(target.position, data.rotationSpeed);
        //Check if we can move "data.moveSpeed" units away.
        //	We chose this distancec, because this is how far we move in 1 second,
        //	this means, we are looking for collisions "one second in the future."
        if (CanMove(data.moveSpeed))
        {
            motor.Move(data.moveSpeed);
        }
        else
        {
            // Enter obstacle avoidance stage 1
            avoidanceStage = 1;
        }
    }

    bool CanMove(float speed)
    {
        // Cast a ray forward in the distance that we sent in
        // If our raycast hit something
        RaycastHit hit;
        if (Physics.Raycast(tankTransform.position, tankTransform.forward, out hit, speed))
        { // if what we hit is not the player
            if (!hit.collider.CompareTag("Player"))
            {
                return false;
            }
        }

        //otherwise
        return true;
    }
}
