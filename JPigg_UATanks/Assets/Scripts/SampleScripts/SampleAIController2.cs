﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAIController2 : MonoBehaviour
{
    public enum AttackMode { Chase, Flee };
    public AttackMode attackMode;
    public Transform target;
    public float fleeDistance = 1.0f;
    private TankData data;
    private TankMotor motor;
    private Transform tankTransform;

    // Use this for initialization
    void Start()
    {
        data = gameObject.GetComponent<TankData>();
        motor = gameObject.GetComponent<TankMotor>();
        tankTransform = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (attackMode == AttackMode.Chase)
        {
            //Rotate towards target
            motor.RotateTowards(target.position, data.rotationSpeed);

            // Move Forward
            motor.Move(data.moveSpeed);
        }
        else if (attackMode == AttackMode.Flee)
        {
            // The vector from ai to target is target position minus our position
            DoFlee();
        }
    }

    private void DoFlee()
    {
        Vector3 vectorToTarget = target.position - tankTransform.position;

        // we can flip this vector by -1 to get a vector AWAY from our target
        Vector3 vectorFromTarget = -1 * vectorToTarget;

        //Now, we can normalize that vector to give it a magnitude of 1
        vectorFromTarget.Normalize();

        // A normalized vector can be multiplied by a length to make a vector of that length.
        vectorFromTarget *= fleeDistance;

        // We can find the position in space we want to move to by adding our vector away from our AI to our Ai's position.
        // This gives us a point that is "that vector away" from our current position.
        Vector3 fleePosition = vectorFromTarget + tankTransform.position;
        motor.RotateTowards(fleePosition, data.rotationSpeed);
        motor.Move(data.moveSpeed);
    }
}
