﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAIController4 : MonoBehaviour
{
    public enum AIState { Chase, ChaseAndFire, CheckForFlee, Flee, Rest };
    public AIState aIState = AIState.Chase;
    public float stateEnterTime;
    public float aiSenseRadius;
    public float restingHealRate; // in hp/second
    public Transform target;
    public float avoidanceTime = 2.0f;
    public float fleeDistance = 1.0f;

    private TankData data;
    private TankMotor motor;
    private Transform tankTransform;
    private int avoidanceStage = 0;
    private float exitTime;
    private float lastShootTime;

    // Use this for initialization
    void Start()
    {
        data = gameObject.GetComponent<TankData>();
        motor = gameObject.GetComponent<TankMotor>();
        tankTransform = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (aIState == AIState.Chase)
        {
            // perform behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();
            }

            // Check for Transitions
            if (data.tankHealth < data.startingHealth * 0.5f)
            {
                ChangeState(AIState.CheckForFlee);
            }
            else if (Vector3.Distance(target.position, tankTransform.position) <= aiSenseRadius)
            {
                ChangeState(AIState.ChaseAndFire);
            }
        }
        else if (aIState == AIState.ChaseAndFire)
        {
            // Perform behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();

                // Limit our firing rate so we can only shoot if enough time has passed
                if (Time.time > lastShootTime + data.reloadTime)
                {
                    motor.Shoot(data.shellPrefab, data.shotForce, data.cannonPosition);
                    lastShootTime = Time.time;
                }
            }

            // Check for Transitions
            if (data.tankHealth < data.startingHealth * 0.5f)
            {
                ChangeState(AIState.CheckForFlee);
            }
            else if (Vector3.Distance(target.position, tankTransform.position) > aiSenseRadius)
            {
                ChangeState(AIState.Chase);
            }
        }
        else if (aIState == AIState.Flee)
        {
            // Perform Behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoFlee();
            }

            // Check for Transition
            if (Time.time >= stateEnterTime + 30)
            {
                ChangeState(AIState.CheckForFlee);
            }
        }
        else if (aIState == AIState.CheckForFlee)
        {
            // Perform Behaviors
            CheckForFlee();

            // Check for Transitions
            if (Vector3.Distance(target.position, tankTransform.position) <= aiSenseRadius)
            {
                ChangeState(AIState.Flee);
            }
            else
            {
                ChangeState(AIState.Rest);
            }
        }
        else if (aIState == AIState.Rest)
        {
            // Perform Behaviors
            DoRest();

            // Check for Transitions
            if (Vector3.Distance(target.position, tankTransform.position) <= aiSenseRadius)
            {
                ChangeState(AIState.Flee);
            }
            else if (data.tankHealth >= data.startingHealth)
            {
                ChangeState(AIState.Chase);
            }
        }
    }



    void DoAvoidance()
    {
        if (avoidanceStage == 1)
        {
            // Rotate left
            motor.Turn(-1 * data.rotationSpeed);

            // If I can now move forward, move to stage 2!
            if (CanMove(data.moveSpeed))
            {
                avoidanceStage = 2;

                // Set the number of seconds we will stay in Stage2
                exitTime = avoidanceTime;
            }

            // Otherwise, we'll do this again next turn!
        }
        else if (avoidanceStage == 2)
        {
            // if we can move forward, do so
            if (CanMove(data.moveSpeed))
            {
                // Subtract from our timer and move
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);

                // If we have moved lond enough, return to chase mode
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                // Otherwise, we can't move forward, so back to stage 1
                avoidanceStage = 1;
            }
        }
    }

    void DoChase()
    {
        motor.RotateTowards(target.position, data.rotationSpeed);
        //Check if we can move "data.moveSpeed" units away.
        //	We chose this distancec, because this is how far we move in 1 second,
        //	this means, we are looking for collisions "one second in the future."
        if (CanMove(data.moveSpeed))
        {
            motor.Move(data.moveSpeed);
        }
        else
        {
            // Enter obstacle avoidance stage 1
            avoidanceStage = 1;
        }
    }

    void DoFlee()
    {
        Vector3 vectorToTarget = target.position - tankTransform.position;

        // we can flip this vector by -1 to get a vector AWAY from our target
        Vector3 vectorFromTarget = -1 * vectorToTarget;

        //Now, we can normalize that vector to give it a magnitude of 1
        vectorFromTarget.Normalize();

        // A normalized vector can be multiplied by a length to make a vector of that length.
        vectorFromTarget *= fleeDistance;

        // We can find the position in space we want to move to by adding our vector away from our AI to our Ai's position.
        // This gives us a point that is "that vector away" from our current position.
        Vector3 fleePosition = vectorFromTarget + tankTransform.position;
        motor.RotateTowards(fleePosition, data.rotationSpeed);
        motor.Move(data.moveSpeed);
    }

    bool CanMove(float speed)
    {
        // Cast a ray forward in the distance that we sent in
        // If our raycast hit something
        RaycastHit hit;
        if (Physics.Raycast(tankTransform.position, tankTransform.forward, out hit, speed))
        { // if what we hit is not the player
            if (!hit.collider.CompareTag("Player"))
            {
                return false;
            }
        }

        //otherwise
        return true;
    }

    public void CheckForFlee()
    {

    }

    public void DoRest()
    {
        // Increase our health. Remember that our increase is "per second"!
        data.tankHealth += restingHealRate * Time.deltaTime;

        // But never go over our max health
        data.tankHealth = Mathf.Min(data.tankHealth, data.startingHealth);
    }

    public void ChangeState(AIState newState)
    {
        // Change our state
        aIState = newState;

        // save the time we changed states
        stateEnterTime = Time.time;
    }
}
