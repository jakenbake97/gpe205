﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class TankData : MonoBehaviour
{
    [HideInInspector] public float moveSpeed = 3f; // the speed at which the tank will move forward and backward in meters per second.
                                                   // Hidden in Inspector so designers will set the property through the GameManager

    [HideInInspector] public float rotationSpeed = 180f; // the speed at which the tank will rotate around the y-axis in degrees per second.
                                                         // Hidden in Inspector so designers will set the property through the GameManager
    [HideInInspector] public GameObject shellPrefab; // The prefab for the shell the tank will shoot
                                                     // Hidden in Inspector so designers will set the property through the GameManager
    [HideInInspector] public float shotForce = 300f; // The amount of force the shell will be shot at
                                                     // Hidden in Inspector so designers will set the property through the GameManager
    [HideInInspector] public float reloadTime = 2.5f; // The minimum amount of time between allowing shots from this tank
                                                      // Hidden in Inspector so designers will set the property through the GameManager                                                     
    [HideInInspector] public float tankHealth = 100f; // The health of the tank
                                                      // Hidden in Inspector so designers will set the property through the GameManager                                                  
    [HideInInspector] public float attackDamage = 20f; // The damage each shell does when it hits another tank                                                      
                                                       // Hidden in Inspector so designers will set the property through the GameManager             
    [HideInInspector] public float shellTimeout = 3f; // The amount of time before the shell is destroyed if it doesnt hit something
                                                      // Hidden in Inspector so designers will set the property through the GameManager                         
    [HideInInspector] public int destroyScore = 50; // The score a tank gets for destroying this tank
                                                    // Hidden in Inspector so designers will set the property through the GameManager
    [HideInInspector] public float startingHealth; // The health a tank starts at, used to compare if the the tank has healed enough or not
                                                   // Hidden in Inspector so it will not be changed. Only set in start and used as a reference by the tankData
    [HideInInspector] public Color matColor; // The color the tank's material will be set to depending on its personality
                                             // Hidden in Inspector so designers will set the property through the GameManager
    [HideInInspector] public GameManager.Personality personality; // reference to the personality enum, can be used to set the tank personality, based on the inspector
                                                                  // Hidden in Inspector so it will not be changed. Only set in start and used as a reference by the 
                                                                  // AI controller
    [HideInInspector] public Transform[] patrolPoints; // the array which holds the patrol points.
                                                       // hidden in the inspector so designers will set the property through the gamemanager
    [HideInInspector] public float hearingRadius; // sets the radius of the sphere trigger used for hearing
                                                  // Hidden in Inspector so designers will set the property through the GameManager
    [HideInInspector] public Transform[] playerPositions; // Holds all of the player positions to be referenced by the AIController
                                                          // Hidden in Inspector so this value is not touched. Should only be set in GameManager                                                  
    [HideInInspector] public float fieldOfView; // the field of view this tank has     
                                                // Hidden in Inspector so designers will set the property through the GameManager                                                     
    [HideInInspector] public float attackRange; // The maximum distance a target can be for the AI to attack                                                
                                                // Hidden in Inspector so designers will set the property through the GameManager 
    [HideInInspector] public float fleeDistance; // The distance a tank should flle away from the player 
                                                 // Hidden in Inspector so designers will set the property through the GameManager                                               
    public float score = 0; // the score value for each tank                                                       

    public Transform cannonPosition; // The end of the cannon, where the shell will be instantiated
    public Renderer bodyRenderer; // The render of the body of the tank. Used to set the tank material based on the personality.
    public AudioClip hitSound; // the sound when the tank is hit
    public AudioClip destroySound; // the sound when the tank is destoryed
    public AudioMixerGroup mixerGroup; // the mixer grou the sounds are on
    GameObject[] playerGameObjects; // the array of players in the game

    // Use this for initializations
    void Start()
    {
        playerGameObjects = new GameObject[GameManager.instance.playerTanks.Length]; // an array of playerGameObjects which is used to temporarily store the reference to the player
                                                                                     // to then get it's transform/
        playerPositions = new Transform[playerGameObjects.Length]; // Creates an array of transforms that is the size of the playerGameObjects array
        for (int i = 0; i < playerGameObjects.Length; i++) // cycle through all of the players and get each's transform
        {
            if (playerGameObjects[i] != null)
            {
                playerPositions[i] = playerGameObjects[i].GetComponent<Transform>();
            }
        }
        startingHealth = tankHealth; // Sets the starting health to be referenced by the tactitician
        Material personalityMat = new Material(Shader.Find("Standard")); // creates a new material using the standard shader
        personalityMat.color = matColor; // sets the color of the material equal to the color set in GameManager for each role
        bodyRenderer.material = personalityMat; // sets the material on the body renderer to be the new material we just created
        if (gameObject.tag == "Tank") // This sets the hearing radius on all gameobjects tagged Tank. Meaning only the enemies and not the players
        {
            gameObject.GetComponent<SphereCollider>().radius = hearingRadius; // sets the sphere collider to be the size of the hearing radius
        }
    }

    // Update is called once per frame
    void Update()
    {
        playerGameObjects = GameManager.instance.playerTanks;
        for (int i = 0; i < playerGameObjects.Length; i++) // cycle through all of the players and get each's transform
        {
            if (playerPositions[i] == null) // if we don't have the player transform
            {
                if (playerGameObjects[i] != null) // but we have the player game object
                {
                    playerPositions[i] = playerGameObjects[i].GetComponent<Transform>(); // get the transform
                }
            }
        }
    }

    public int TakeDamage(float damage) // called by the DestroyFiredShell script and passes the amount of damage the shell does
    {
        tankHealth -= damage; // subtract the tankHealth value by the damage of the shell
        if (hitSound != null)
        {
            GameObject oneShotSound = new GameObject("One Shot Sound"); // create a new gameobject
            oneShotSound.transform.position = transform.position; // at this position
            oneShotSound.AddComponent<AudioSource>(); // and add an audio source to it
            AudioSource source = oneShotSound.GetComponent<AudioSource>(); // get teh audio source
            source.clip = hitSound; // clip equals the hitsound
            source.outputAudioMixerGroup = mixerGroup; // set it to the fx mixer group
            source.volume = 0.7f; // set its volume to 70%
            source.Play(); // play the clip
            Destroy(oneShotSound, hitSound.length); // destroy the new gameobject after the clip
        }
        if (tankHealth <= 0) // if the tank's health gets below 0, destroy the tank gameObject and return the score value to the shell
        {
            if (destroySound != null)
            {
                // do the same as above when the tank is destroyed
                GameObject oneShotSound = new GameObject("One Shot Sound");
                oneShotSound.transform.position = transform.position;
                oneShotSound.AddComponent<AudioSource>();
                AudioSource source = oneShotSound.GetComponent<AudioSource>();
                source.clip = destroySound;
                source.outputAudioMixerGroup = mixerGroup;
                source.volume = 0.7f;
                source.Play();
                Destroy(oneShotSound, destroySound.length);
            }

            Destroy(gameObject);
            return destroyScore;
        }
        return 0; // if the tank isnt destroyed return 0 so no points are added
    }

    public void AddScore(int points) // adds the score using the passed value points from the shell this tank shot
    {
        score += points; // score equals current score pluss the new points
    }
}
