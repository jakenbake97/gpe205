﻿using UnityEngine;
using System.Collections;

public class Room : MonoBehaviour
{

    public GameObject doorNorth;
    public GameObject doorSouth;
    public GameObject doorEast;
    public GameObject doorWest;
    public Transform[] playerSpawn;
    public Transform enemySpawn;
    public Transform powerUpSpawn;
    [HideInInspector] public GameObject pickupPrefab;
    public Transform[] waypoints;
    float spawnDelay;
    float nextSpawnTime;
    GameObject spawnedPickup;
    Transform roomTF;

    void Start()
    {
        nextSpawnTime = Time.time;
        spawnDelay = pickupPrefab.gameObject.GetComponent<Pickup>().powerup.respawnDelay;
        roomTF = gameObject.GetComponent<Transform>();
    }

    void Update()
    {
        if (spawnedPickup == null)
        {
            if (Time.time > nextSpawnTime)
            {
                spawnedPickup = Instantiate(pickupPrefab, powerUpSpawn.position, Quaternion.identity);
                spawnedPickup.transform.SetParent(roomTF);
                nextSpawnTime = Time.time + spawnDelay;
            }
        }
        else
        {
            nextSpawnTime = Time.time + spawnDelay;
        }
    }




}
